//
//  ResourcesManagers.swift
//  Old MacDonald
//
//  Created by Joaquin Rocco on 09/06/2016.
//  Copyright © 2016 Alvarellos Duran Rocco. All rights reserved.
//

import UIKit
import Foundation

class ImageManager {
    static func getLarge(forAnimal animal: Animal) -> UIImage{
        switch animal {
        case .Chick:
            return UIImage.init(named: "chick")!
        case .Lion:
            return UIImage.init(named: "lion")!
        case .Pig:
            return UIImage.init(named: "pig")!
        case .Cow:
            return UIImage.init(named: "cow")!
        case .Dog:
            return UIImage.init(named: "dog")!
        case .Monkey:
            return UIImage.init(named: "monkey")!
        case .Elephant:
            return UIImage.init(named: "elephant")!
        case .Duck:
            return UIImage.init(named: "duck")!
        case .Cat:
            return UIImage.init(named: "cat")!
        }
    }
    
    static func getThumbnail(forAnimal animal: Animal) -> UIImage{
        switch animal {
        case .Chick:
            return UIImage.init(named: "head_chick")!
        case .Lion:
            return UIImage.init(named: "head_lion")!
        case .Pig:
            return UIImage.init(named: "head_pig")!
        case .Cow:
            return UIImage.init(named: "head_cow")!
        case .Dog:
            return UIImage.init(named: "head_dog")!
        case .Monkey:
            return UIImage.init(named: "head_monkey")!
        case .Elephant:
            return UIImage.init(named: "head_elephant")!
        case .Duck:
            return UIImage.init(named: "head_duck")!
        case .Cat:
            return UIImage.init(named: "head_cat")!
        }
    }
    
    static func getButtonBaseImage(state: UIControlState) -> UIImage{
        switch state {
        case UIControlState.Normal:
            return UIImage(named: "button_normal")!
        default:
            return UIImage(named: "button_pressed")!
        }
    }
    
    static func getStartButtonBaseImage(state: UIControlState) -> UIImage{
        switch state{
        case UIControlState.Normal:
            return UIImage(named: "play_button_normal")!
        default:
            return UIImage(named: "play_button_pressed")!
        }
    }
}