//
//  Helpers.swift
//  Old MacDonald
//
//  Created by Joaquin Rocco on 11/06/2016.
//  Copyright © 2016 Alvarellos Duran Rocco. All rights reserved.
//

import Foundation
import UIKit

enum Animal:String {
    case Chick
    case Lion
    case Pig
    case Cow
    case Dog
    case Monkey
    case Elephant
    case Duck
    case Cat
}

struct FontManager{
    
    static let gameFontColor = UIColor(colorLiteralRed: 255/255, green: 249/255, blue: 0/255, alpha: 1)
    
    static let pointsColor = UIColor(colorLiteralRed: 1, green: 84/255, blue: 93/255, alpha: 1)
    
    static func gameFont(withSize size: CGFloat) -> UIFont {
        return UIFont(name: "GROBOLD", size: size)!
    }
    
    static func addFontShadow(layer: CALayer){
        layer.shadowColor = UIColor.blackColor().CGColor
        layer.shadowOffset = CGSize(width: 0, height: 4)
        layer.shadowOpacity = 0.5
        layer.shadowRadius = 0
        layer.masksToBounds = false
    }
}

struct Notifications{
    static let MainThemeEnded = "MainThemeEnded"
}

struct Highscore{
    var score: Int
    var finalLevel: Int
    var finalAnimal: Animal
}

// MARK - Extensions

extension Array where Element:Equatable{
    func randomElement() -> Element{
        let randomIndex = Int(arc4random_uniform(UInt32(self.count)))
        return self[randomIndex]
    }
    
    func randomElement(diferentFrom e: Element) -> Element{
        if self.count > 1{
            var elem = e
            while elem == e{
                elem = self.randomElement()
            }
            return elem
        }else{
            return self[0]
        }
    }
}

public extension Int {
    /// Returns a random Int point number between 0 and Int.max.
    public static var random:Int {
        get {
            return Int.random(Int.max)
        }
    }
    /**
     Random integer between 0 and n-1.
     
     - parameter n: Int
     
     - returns: Int
     */
    public static func random(n: Int) -> Int {
        return Int(arc4random_uniform(UInt32(n)))
    }
    /**
     Random integer between min and max
     
     - parameter min: Int
     - parameter max: Int
     
     - returns: Int
     */
    public static func random(min: Int, max: Int) -> Int {
        return Int.random(max - min + 1) + min
        //Int(arc4random_uniform(UInt32(max - min + 1))) + min }
    }
}