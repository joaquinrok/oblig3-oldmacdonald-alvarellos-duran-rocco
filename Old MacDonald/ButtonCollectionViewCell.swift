//
//  ButtonCollectionViewCell.swift
//  Old MacDonald
//
//  Created by Joaquin Rocco on 09/06/2016.
//  Copyright © 2016 Alvarellos Duran Rocco. All rights reserved.
//

import UIKit

class ButtonCollectionViewCell: UICollectionViewCell, AnimalButtonDelegate {
    
    var animal: Animal!{
        didSet{
            button.animalTag = animal.rawValue
//            button.setTitle(animal.rawValue, forState: .Normal)
        }
    }
    @IBOutlet weak var button: AnimalButton!{
        didSet{
            button.delegate = self
        }
    }
    
    func buttonPressed(sender: AnimalButton) {
        SoundManager.sharedInstance.playSound(forAnimal: animal)
    }
    
}
