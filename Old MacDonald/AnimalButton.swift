//
//  AnimalButton.swift
//  Old MacDonald
//
//  Created by Joaquin Rocco on 09/06/2016.
//  Copyright © 2016 Alvarellos Duran Rocco. All rights reserved.
//

import UIKit

class AnimalButton: UIView {
    
    var delegate: AnimalButtonDelegate?
    
    var animalTag: String?{
        didSet{
            if let animal = Animal(rawValue: animalTag!){
                animalImageView.image = ImageManager.getThumbnail(forAnimal: animal)
            }
        }
    }
    
    var buttonImageView: UIImageView!
    var animalImageView: UIImageView!
    
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
        setupButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupButton()
    }
    
    func setupButton(){
        buttonImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        buttonImageView.image = ImageManager.getButtonBaseImage(.Normal)
        addSubview(buttonImageView!)
        animalImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        addSubview(animalImageView)
    }
    
    override func layoutSubviews() {
        buttonImageView.frame = CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height)
        let size = frame.width * 0.7
        let margin = (frame.width - size) * 0.5
        animalImageView.frame = CGRect(x: margin, y: margin*0.8, width: size, height: size)
    }
    
    private let yChange: CGFloat = 10
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        buttonImageView.image = ImageManager.getButtonBaseImage(.Selected)
        var f = animalImageView.frame
        f.origin.y += yChange
        animalImageView.frame = f
        setNeedsDisplay()
        delegate?.buttonPressed(self)
        if let tag = animalTag{
            targetAction(animalTag: tag)
        }
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        restoreButtonBackground()
    }
    
    override func touchesCancelled(touches: Set<UITouch>?, withEvent event: UIEvent?) {
        restoreButtonBackground()
    }
    
    func restoreButtonBackground(){
        buttonImageView.image = ImageManager.getButtonBaseImage(.Normal)
        var f = animalImageView.frame
        f.origin.y -= yChange
        animalImageView.frame = f
        setNeedsDisplay()
    }
    
    var targetAction:(animalTag: String)->Void = {_ in}
    func addTarget(targetAction: (animalTag: String)->Void ){
        self.targetAction = targetAction
    }
}

protocol AnimalButtonDelegate{
    func buttonPressed(sender: AnimalButton)
}
