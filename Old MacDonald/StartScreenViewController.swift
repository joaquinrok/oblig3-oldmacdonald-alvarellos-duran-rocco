//
//  StartScreenViewController.swift
//  Old MacDonald
//
//  Created by Andoni Alvarellos on 6/17/16.
//  Copyright © 2016 Alvarellos Duran Rocco. All rights reserved.
//

import UIKit

let segueIdToGameScreen = "ToGameScreen"

class StartScreenViewController: UIViewController {
    
    @IBAction func playButtonPressed(sender: UIButton) {        
        performSegueWithIdentifier(segueIdToGameScreen, sender: nil)
    }
}
