//
//  LargeButton.swift
//  Old MacDonald
//
//  Created by Joaquin Rocco on 26/06/2016.
//  Copyright © 2016 Alvarellos Duran Rocco. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class LargeButton: UIButton{
    
    @IBInspectable
    var fontSize: CGFloat = 56{
        didSet{
            textLabel.font = FontManager.gameFont(withSize: fontSize)
        }
    }
    
    @IBInspectable
    var text: String?{
        didSet{
            textLabel?.text = text
        }
    }
    var textLabel: UILabel!
    @IBInspectable
    var textColor: UIColor?{
        didSet{
            textLabel?.textColor = textColor
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        self.setBackgroundImage(UIImage(named: "button_large_normal"), forState: .Normal)
        self.setBackgroundImage(UIImage(named: "button_large_pressed"), forState: .Highlighted)
        textLabel = UILabel(frame: CGRect(x: 0, y: 0, width: frame.width, height: frame.height))
        textLabel.font = FontManager.gameFont(withSize: 56)
        textLabel.textAlignment = NSTextAlignment.Center
        addSubview(textLabel)
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        textLabel.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height)
    }
    
    let yChange: CGFloat = 10
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesBegan(touches, withEvent: event)
        var f = textLabel.frame
        f.origin.y += yChange
        textLabel.frame = f
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        restoreButtonBackground()
        super.touchesEnded(touches, withEvent: event)
    }
    
    override func touchesCancelled(touches: Set<UITouch>?, withEvent event: UIEvent?) {
        restoreButtonBackground()
        super.touchesCancelled(touches, withEvent: event)
    }
    
    func restoreButtonBackground(){
        var f = textLabel.frame
        f.origin.y -= yChange
        textLabel.frame = f
    }
    
}
