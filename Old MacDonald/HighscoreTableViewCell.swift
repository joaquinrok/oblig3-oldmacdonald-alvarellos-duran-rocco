//
//  HighscoreTableViewCell.swift
//  Old MacDonald
//
//  Created by Andoni Alvarellos on 6/17/16.
//  Copyright © 2016 Alvarellos Duran Rocco. All rights reserved.
//

import UIKit

class HighscoreTableViewCell: UITableViewCell {

    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
}
