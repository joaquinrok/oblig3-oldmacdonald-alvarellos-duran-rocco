//
//  GameEngine.swift
//  Old MacDonald
//
//  Created by Joaquin Rocco on 09/06/2016.
//  Copyright © 2016 Alvarellos Duran Rocco. All rights reserved.
//

import Foundation
import AVFoundation

protocol GameEngineDelegate {
    
    func setupLevel(forLevel level: Int, withTimeforIntro time: Double, andPoints points: Int)
    func animalSelected(forAnimal: Animal, withPoints points: Int, totalPoints: Int)
}

class GameEngine{
    
    let levelDelegate: GameEngineDelegate
    
    var animals: [Animal]
    
    var currentAnimal: Animal!
    var currentLevel: Int = 1
    var levelDate: NSDate!
    var tapCount = 0
    
    var totalPoints = 0
    
    var gameIsOver = false
    
    var nameTimer: NSTimer!
    
    init(animals: [Animal], levelController: GameEngineDelegate){
        self.levelDelegate = levelController
        self.animals = animals
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(loadNextLevel), name: Notifications.MainThemeEnded, object: nil)
    }
    
    func restartGame(){
        gameIsOver = false
        currentLevel = 1
        totalPoints = 0
        setupLevel()
    }
    
    func setupLevel(){
        if let currentAnimal = currentAnimal{
            self.currentAnimal = animals.randomElement(diferentFrom: currentAnimal)
        }else{
            currentAnimal = animals.randomElement()
        }
        
        SoundManager.sharedInstance.playTheme(withSpeed: speed(forLevel: currentLevel))
        let nameTime = Double(11.8 / speed(forLevel: currentLevel))
        nameTimer = NSTimer.scheduledTimerWithTimeInterval(nameTime, target: self, selector: #selector(playAnimalName), userInfo: nil, repeats: false)
        levelDate = NSDate() //restart timer
        tapCount = 0
        
        levelDelegate.setupLevel(forLevel: currentLevel, withTimeforIntro: normalizeWithSpeed(13, forLevel: currentLevel), andPoints: totalPoints)
    }
    
    @objc func playAnimalName(){
        SoundManager.sharedInstance.playName(forAnimal: currentAnimal)
    }
    
    @objc func loadNextLevel(){
        if !gameIsOver {
            currentLevel += 1
            print("NEXT LEVEL! #\(currentLevel)")
            setupLevel()
        }
    }
    
    func animalSelected(animalTag: String){
        var points = -1
        let elapsedTime = NSDate().timeIntervalSinceDate(levelDate)
        if animalTag == currentAnimal.rawValue{
            points = hitPoints(tapCount, tapTime: elapsedTime)
            print("Correct animal = \(points) pts.")
        }else{
            print("Wrong animal")
        }
        tapCount += 1
        if points >= 0 {
            self.totalPoints += points
        } else {
            gameOver()
        }
        levelDelegate.animalSelected(Animal(rawValue: animalTag)!, withPoints: points, totalPoints: totalPoints)
    }
    
    private let hitTimes = [15.23, 15.50, 16.40, 16.77, 18.06, 18.69, 19.61, 19.94]
    private let themeLength = 25.251
    private let maxTimeDiference = 0.8
    
    private let speedIncrement: Float = 0.2
    private func speed(forLevel level: Int) -> Float{
        return (1-speedIncrement) + Float(level) * speedIncrement
    }
    
    func normalizeWithSpeed(time: NSTimeInterval, forLevel level: Int) -> Double{
        return time/Double(speed(forLevel: level))
    }
    
    func hitPoints(tapIndex: Int, tapTime: NSTimeInterval) -> Int{
        guard tapIndex < hitTimes.count else{ return -1 }
        let tap = hitTimes[tapIndex]
        let normalizedTap = normalizeWithSpeed(tap, forLevel: currentLevel)
        print("tap \(tapIndex) at: \(tapTime) - target: \(normalizedTap)")
        let timeDiference = abs(normalizedTap - tapTime)
        if timeDiference <= normalizeWithSpeed(maxTimeDiference, forLevel: currentLevel){
            return Int(100/(timeDiference+0.00001))
        }else{
            return -1
        }
    }
    
    func gameOver(){
        gameIsOver = true
        SoundManager.sharedInstance.stopMusic()
        nameTimer.invalidate()
        
        let finalPoints = totalPoints
        let finalLevel = currentLevel
        
        let highscore: [String:AnyObject] = ["score": finalPoints, "level": finalLevel]
        
        if var currentHighscores: [[String: AnyObject]] = NSUserDefaults.standardUserDefaults().objectForKey("highscores") as? [[String: AnyObject]] {
            if finalPoints > currentHighscores.first!["score"] as! Int || currentHighscores.count < 10{
                if currentHighscores.count == 10{
                    currentHighscores.removeFirst()
                }
                var currentIndex = 0
                var reachedHigher = false
                for var hs in currentHighscores{
                    if hs["score"] as! Int > finalPoints{
                        currentHighscores.insert(highscore, atIndex: currentIndex)
                        reachedHigher = true
                        break
                    }
                    else{
                        currentIndex += 1
                    }
                }
                
                if(!reachedHigher){
                    currentHighscores.append(highscore)
                }
                NSUserDefaults.standardUserDefaults().setObject(currentHighscores, forKey: "highscores")
            }
        }
        else{
            NSUserDefaults.standardUserDefaults().setObject([highscore], forKey: "highscores")
        }
    }
}