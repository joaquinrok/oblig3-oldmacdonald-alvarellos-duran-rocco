//
//  LoseViewController.swift
//  Old MacDonald
//
//  Created by Andoni Alvarellos on 6/22/16.
//  Copyright © 2016 Alvarellos Duran Rocco. All rights reserved.
//

import UIKit

protocol LoseViewControllerDelegate {
    func restartGame()
}

class LoseViewController: UIViewController {
    
    var delegate: LoseViewControllerDelegate?
    
    private let yChange: CGFloat = 5
    //private var parentController: ViewController!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    
    var points: Int = 0

    @IBAction func shareButtonPressed(sender: UIButton) {
        sender.setBackgroundImage(ImageManager.getButtonBaseImage(.Normal), forState: sender.state)
        let textToShare = "I scored " + String(self.points) + " points in the OldMcDonald game! Think you can beat me?"
        
        let activityVC = UIActivityViewController(activityItems: [textToShare], applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = sender
        self.presentViewController(activityVC, animated: true, completion: nil)
    }
    @IBAction func highscoresButtonPressed(sender: UIButton) {
        sender.setBackgroundImage(ImageManager.getButtonBaseImage(.Normal), forState: sender.state)
        parentViewController?.performSegueWithIdentifier(segueIdToHighscoresScreen, sender: nil)
    }
    
    @IBAction func highscoresButtonTouchBegan(sender: UIButton) {
        sender.setBackgroundImage(ImageManager.getButtonBaseImage(.Selected), forState: sender.state)
        var f = sender.imageView!.frame
        f.origin.y += yChange
        sender.imageView!.frame = f

    }
    
    @IBAction func shareButtonTouchBegan(sender: UIButton) {
        sender.setBackgroundImage(ImageManager.getButtonBaseImage(.Selected), forState: sender.state)
        var f = sender.imageView!.frame
        f.origin.y += yChange
        sender.imageView!.frame = f
    }
    
    @IBAction func replayButtonPressed(sender: UIButton) {
        delegate = parentViewController as! GameViewController
        delegate?.restartGame()
    }
    
    func setScore(points: Int){
        self.points = points
        scoreLabel.text = "You scored "+String(self.points)+" points"
    }
}
