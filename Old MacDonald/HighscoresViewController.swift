//
//  HighscoresViewController.swift
//  Old MacDonald
//
//  Created by Andoni Alvarellos on 6/17/16.
//  Copyright © 2016 Alvarellos Duran Rocco. All rights reserved.
//

import UIKit

class HighscoresViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{

    @IBOutlet weak var highscoresTableView: UITableView!
    
    var highscores: [[String: AnyObject]] = []
    
    override func viewDidLoad() {
        highscoresTableView.delegate = self
        highscoresTableView.dataSource = self
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        highscores = NSUserDefaults.standardUserDefaults().objectForKey("highscores") as! [[String: AnyObject]]
        highscores = highscores.reverse()
        highscoresTableView.reloadData()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return highscores.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("HighscoreTableViewCell", forIndexPath: indexPath) as! HighscoreTableViewCell
        
        cell.scoreLabel.text = "Score: " + String(highscores[indexPath.row]["score"]!)
        cell.levelLabel.text = "Level " + String(highscores[indexPath.row]["level"]!)
        return cell
    }
    
    
    @IBAction func backButtonPressed(sender: UIButton) {
        navigationController?.popViewControllerAnimated(true)
    }
    
}
