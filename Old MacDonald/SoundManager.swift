//
//  SoundManager.swift
//  Old MacDonald
//
//  Created by Joaquin Rocco on 11/06/2016.
//  Copyright © 2016 Alvarellos Duran Rocco. All rights reserved.
//

import Foundation
import AVFoundation

class SoundManager {
    
    static let sharedInstance = SoundManager()
    
    var audioEngine: AVAudioEngine = AVAudioEngine()
    var themePlayer = AVAudioPlayerNode()
    var themeVarispeedUnit = AVAudioUnitVarispeed()
    var nameVarispeedUnit = AVAudioUnitVarispeed()
    let themeBuffer: AVAudioPCMBuffer
    var animalPlayer = AVAudioPlayerNode()
    var namePlayer = AVAudioPlayerNode()
    
    private init(){
        // Main Theme
        let audioFile = AVAudioFile(forReadingPath: "theme", ofType: "caf")
        themeBuffer = AVAudioPCMBuffer(audioFile: audioFile)
        try! audioFile.readIntoBuffer(themeBuffer)
        
        // Attach Audio Nodes to Engine
        audioEngine.attachNode(themePlayer)
        audioEngine.attachNode(animalPlayer)
        audioEngine.attachNode(namePlayer)
        audioEngine.attachNode(themeVarispeedUnit)
        audioEngine.attachNode(nameVarispeedUnit)
        
        // Setup Engine routing
        let mainMixer = audioEngine.mainMixerNode
        let outputFormat = AVAudioFormat(standardFormatWithSampleRate: audioFile.fileFormat.sampleRate, channels: 2)
        audioEngine.connect(themePlayer, to: themeVarispeedUnit, format: outputFormat)
        audioEngine.connect(namePlayer, to: nameVarispeedUnit, format: outputFormat)
        audioEngine.connect(themeVarispeedUnit, to:mainMixer, format: outputFormat)
        audioEngine.connect(nameVarispeedUnit, to:mainMixer, format: outputFormat)
        audioEngine.connect(animalPlayer, to: mainMixer, format: outputFormat)
        
        try! audioEngine.start()
    }
    
    func playTheme(withSpeed speed: Float){
        themeVarispeedUnit.rate = speed
        nameVarispeedUnit.rate = speed
        themePlayer.scheduleBuffer(themeBuffer){
            dispatch_async(dispatch_get_main_queue()){
                NSNotificationCenter.defaultCenter().postNotificationName(Notifications.MainThemeEnded, object: nil)
            }
        }
        themePlayer.play()
    }
    
    // Mark - Animal Sounds
    
    func playSound(forAnimal animal: Animal){
        let audioFile: AVAudioFile
        let audioBuffer: AVAudioPCMBuffer
        switch animal {
        case .Cow:
            audioFile = AVAudioFile(forReadingPath: "cow", ofType: "caf")
        case .Cat:
            audioFile = AVAudioFile(forReadingPath: "cat", ofType: "caf")
        case .Chick:
            audioFile = AVAudioFile(forReadingPath: "chick", ofType: "aiff")
        case .Dog:
            audioFile = AVAudioFile(forReadingPath: "dog", ofType: "aiff")
        case .Duck:
            audioFile = AVAudioFile(forReadingPath: "duck", ofType: "aiff")
        case .Elephant:
            audioFile = AVAudioFile(forReadingPath: "elephant", ofType: "aiff")
        case .Lion:
            audioFile = AVAudioFile(forReadingPath: "lion", ofType: "aiff")
        case .Monkey:
            audioFile = AVAudioFile(forReadingPath: "monkey", ofType: "aiff")
        case .Pig:
            audioFile = AVAudioFile(forReadingPath: "pig", ofType: "aiff")
        }
        audioBuffer = AVAudioPCMBuffer(audioFile: audioFile)
        try! audioFile.readIntoBuffer(audioBuffer)
        
        animalPlayer.scheduleBuffer(audioBuffer, atTime: nil, options: .Interrupts, completionHandler: nil)
        animalPlayer.play()
        
    }
    
    func playName(forAnimal animal: Animal){
        let audioFile: AVAudioFile
        let audioBuffer: AVAudioPCMBuffer
        switch animal {
        case .Cow:
            audioFile = AVAudioFile(forReadingPath: "Vaca-nombre", ofType: "aiff")
        case .Cat:
            audioFile = AVAudioFile(forReadingPath: "Gato-nombre", ofType: "aiff")
        case .Chick:
            audioFile = AVAudioFile(forReadingPath: "Gallina-nombre", ofType: "aiff")
        case .Dog:
            audioFile = AVAudioFile(forReadingPath: "Perro-nombre", ofType: "aiff")
        case .Duck:
            audioFile = AVAudioFile(forReadingPath: "Pato-nombre", ofType: "aiff")
        case .Elephant:
            audioFile = AVAudioFile(forReadingPath: "Elefante-nombre", ofType: "aiff")
        case .Lion:
            audioFile = AVAudioFile(forReadingPath: "Leon-nombre", ofType: "aiff")
        case .Monkey:
            audioFile = AVAudioFile(forReadingPath: "Mono-nombre", ofType: "aiff")
        case .Pig:
            audioFile = AVAudioFile(forReadingPath: "Chancho-nombre", ofType: "aiff")
        }
        audioBuffer = AVAudioPCMBuffer(audioFile: audioFile)
        try! audioFile.readIntoBuffer(audioBuffer)
        
        namePlayer.scheduleBuffer(audioBuffer, atTime: nil, options: .Interrupts, completionHandler: nil)
        namePlayer.volume = 4.0
        namePlayer.play()
    }
    
    func stopMusic(){
        themePlayer.stop()
    }
}

extension AVAudioFile{
    convenience init(forReadingPath path: String, ofType type: String){
        try! self.init(forReading: NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource(path, ofType: type)!))
    }
}

extension AVAudioPCMBuffer{
    convenience init(audioFile: AVAudioFile){
        self.init(PCMFormat: audioFile.processingFormat, frameCapacity: UInt32(audioFile.length))
    }
}