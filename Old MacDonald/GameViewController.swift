//
//  ViewController.swift
//  Old MacDonald
//
//  Created by Joaquin Rocco on 08/06/2016.
//  Copyright © 2016 Alvarellos Duran Rocco. All rights reserved.
//

import UIKit

let segueIdToHighscoresScreen = "ToHighscoresScreen"

class GameViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, GameEngineDelegate, UICollectionViewDelegateFlowLayout, LoseViewControllerDelegate {
    
    @IBOutlet weak var buttonCollectionView: UICollectionView!
    @IBOutlet weak var gameOverContainerView: UIView!
    @IBOutlet weak var totalPointsLabel: UILabel!
    @IBOutlet weak var overlay: UIView!
    
    
    var displayWidth: CGFloat!
    var largeImageView: UIImageView?
    
    let animals = [
        Animal.Chick,
        Animal.Lion,
        Animal.Pig,
        Animal.Cow,
        Animal.Dog,
        Animal.Monkey,
        Animal.Elephant,
        Animal.Duck,
        Animal.Cat
    ]
    var engine: GameEngine!
    
    var canvasWidth: CGFloat!
    var canvasHeight: CGFloat!
    
    lazy var animator: UIDynamicAnimator = UIDynamicAnimator(referenceView: self.view)
    var gravity = UIGravityBehavior()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        gameOverContainerView.hidden = true
        overlay.hidden = true
        
        self.engine = GameEngine(animals: animals, levelController: self)
        displayWidth = view.frame.width
        buttonCollectionView.delegate = self
        buttonCollectionView.dataSource = self
        let size = displayWidth*0.7
        largeImageView = UIImageView(frame: CGRect(x: (displayWidth-size)*0.5, y: 70, width: size, height: size))
        
        canvasWidth = view.frame.width
        canvasHeight = view.frame.height * 0.5
        animator.addBehavior(gravity)
        
        engine.restartGame()
    }
    
    // MARK: Button collection
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return animals.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let buttonCell = collectionView.dequeueReusableCellWithReuseIdentifier("buttonCell", forIndexPath: indexPath) as! ButtonCollectionViewCell
        let animal = animals[indexPath.row]
        buttonCell.animal = animal
        buttonCell.button.addTarget({ tag in
            self.engine.animalSelected(tag)
        })
        return buttonCell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let size = buttonCollectionView.frame.height * 0.3
        return CGSizeMake(size, size)
    }
    
    func showMainAnimal(timer: NSTimer){
        let time = timer.userInfo as! Double
        let size = canvasHeight*0.6
        largeImageView?.frame = CGRectMake(canvasWidth, canvasHeight-size-10, size, size)
        largeImageView?.hidden = false
        UIView.animateWithDuration(time*0.2, delay: 0, options: .CurveEaseInOut, animations: {
            self.largeImageView?.center = CGPoint(x: self.canvasWidth/2, y: self.largeImageView!.center.y)
            }, completion: { _ in
                UIView.animateWithDuration(time*0.1, delay: time*0.1, options: .CurveEaseIn, animations: {
                    self.largeImageView?.center = CGPoint(x: self.canvasWidth/2, y: self.canvasHeight-30)
                    self.largeImageView?.transform = CGAffineTransformMakeScale(0.1, 0.1)
                    }, completion: { _ in
                        self.largeImageView?.center = CGPoint(x: 1000, y: 0)
                        self.largeImageView?.transform = CGAffineTransformMakeScale(1, 1)
                })
        })
        largeImageView!.image = ImageManager.getLarge(forAnimal: engine.currentAnimal)
        view.addSubview(largeImageView!)
    }
    
    var animalBuffer = [UIImageView]()
    
    var context: UInt8 = 1
    
    var pointsLabel: UILabel!
    
    func animalSelected(animal: Animal, withPoints points: Int, totalPoints: Int){
        if pointsLabel == nil{
            pointsLabel = createTitleLabel("", size: 46)
            pointsLabel.center = CGPoint(x: pointsLabel.center.x, y:canvasHeight - 40)
            pointsLabel.textColor = FontManager.pointsColor
            self.view.addSubview(pointsLabel)
        }
        
        if points >= 0 {
            // Display points
            pointsLabel.text = "\(points) pts"
            totalPointsLabel.text = "\(totalPoints) pts"
            animateTitleLabel(pointsLabel, delay: 0, transitionDuration: 0.1, shownFor: 0.5)
            
            // Shoot animal
            let size = canvasHeight * 0.45
            
            let image: UIImageView!
            let frame = CGRect(x: (canvasWidth-size)/2, y: canvasHeight-size, width: size, height: size)
            if animalBuffer.count > 0{
                image = animalBuffer.removeLast() as UIImageView
                image.frame = frame
            }else{
                image = UIImageView(frame: frame)
                image.addObserver(self, forKeyPath: "center", options: NSKeyValueObservingOptions.New, context: &context)
            }
            
            image.image = ImageManager.getLarge(forAnimal: animal)
            
            view.addSubview(image)
            gravity.addItem(image)
            
            let push = UIPushBehavior(items: [image], mode: .Instantaneous)
            push.pushDirection = CGVector(dx: Int.random(-5, max: 5), dy: -10)
            let a = Int.random(Int(size/8), max: Int(size/4))
            push.magnitude = CGFloat(a)
            push.action = { push.dynamicAnimator!.removeBehavior(push) }
            animator.addBehavior(push)
        } else {
            // Game Over
            print("Game Over!")
            // Stop all title animations
            levelLabel.hidden = true
            tutorialLabel.hidden = true
            readyLabel.hidden = true
            animalTimer.invalidate()
            largeImageView?.hidden = true
            readyLabel.hidden = true
            if wellDoneLabel != nil {wellDoneLabel.hidden = true}
            if levelPointsLabel != nil {levelPointsLabel.hidden = true}
            
            // Setup and show game over view
            overlay.alpha = 0
            overlay.hidden = false
            
            let child = self.childViewControllers[0] as! LoseViewController
            child.setScore(totalPoints)
            
            UIView.animateWithDuration(0.5, delay: 0, options: .CurveEaseOut, animations: {
                self.overlay.alpha = 0.5
                }, completion:nil )
            gameOverContainerView.center = CGPoint(x: gameOverContainerView.center.x, y: view.frame.height+gameOverContainerView.frame.height/2)
            gameOverContainerView.hidden = false
            UIView.animateWithDuration(0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 10, options: .CurveEaseOut, animations: {
                    self.gameOverContainerView.center = CGPoint(x: self.gameOverContainerView.center.x, y: (self.view.frame.height-self.gameOverContainerView.frame.height)/2+self.gameOverContainerView.frame.height/2)
                }, completion: nil)
            
        }
    }
    
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        if context == &self.context {
            if let image = object as? UIImageView{
                if image.center.y > 1000 {
                    gravity.removeItem(image)
                    animalBuffer.append(image)
                }
            }
        }
    }
    
    @IBAction func viewHighscoresPressed(sender: UIButton) {
        performSegueWithIdentifier(segueIdToHighscoresScreen, sender: nil)
    }
    
    var levelLabel: UILabel!
    var tutorialLabel: UILabel!
    var readyLabel: UILabel!
    var levelPointsLabel: UILabel!
    var wellDoneLabel: UILabel!
    var animalTimer: NSTimer!
    
    func setupLevel(forLevel level: Int, withTimeforIntro time: Double, andPoints points: Int){
        let transitionDuration = time * 0.015
        
        totalPointsLabel.text = "\(points) pts"
        
        if level == 1 {
            levelLabel = createTitleLabel("Level \(level)", size: 56)
            view.addSubview(levelLabel)
            animateTitleLabel(levelLabel,
                              delay: time * 0.03,
                              transitionDuration: transitionDuration,
                              shownFor: time * 0.25)
            
            tutorialLabel = createTitleLabel("Tap the animals in sync with the music!", size: 44)
            view.addSubview(tutorialLabel)
            animateTitleLabel(tutorialLabel,
                              delay: time * 0.35,
                              transitionDuration: transitionDuration,
                              shownFor: time * 0.20)
            
            readyLabel = createTitleLabel("Get ready!", size: 56)
            view.addSubview(readyLabel)
        
        } else {
            if levelPointsLabel == nil {
                levelPointsLabel = createTitleLabel("", size: 56)
                view.addSubview(levelPointsLabel)
            }
            if wellDoneLabel == nil {
                wellDoneLabel = createTitleLabel("", size: 65)
                view.addSubview(wellDoneLabel)
            }
            
            wellDoneLabel.text = "Well done!"
            animateTitleLabel(wellDoneLabel,
                              delay: 0,
                              transitionDuration: transitionDuration,
                              shownFor: time * 0.15)
            
            levelPointsLabel.text = "\(points) pts!"
            animateTitleLabel(levelPointsLabel,
                              delay: time * 0.2,
                              transitionDuration: transitionDuration,
                              shownFor: time * 0.15)
            
            levelLabel.text = "Level \(level)"
            animateTitleLabel(levelLabel,
                              delay: time * 0.38,
                              transitionDuration: transitionDuration,
                              shownFor: time * 0.20)
            
        }
        
        animateTitleLabel(readyLabel,
                          delay: time * 0.6,
                          transitionDuration: transitionDuration,
                          shownFor: time * 0.3)
        
        animalTimer = NSTimer.scheduledTimerWithTimeInterval(time * 0.7, target: self, selector: #selector(self.showMainAnimal), userInfo: time, repeats: false)
    }
    
    private func animateTitleLabel(label: UILabel, delay: Double, transitionDuration: Double, shownFor: Double){
        label.alpha = 0
        UIView.animateWithDuration(transitionDuration, delay: delay, options: .CurveEaseOut, animations: {
            label.alpha = 1
        }){ _ in
            UIView.animateWithDuration(transitionDuration, delay: shownFor, options: .CurveEaseIn, animations: {
                label.alpha = 0
                }, completion: nil)
        }
    }
    
    private func createTitleLabel(text: String, size: CGFloat) -> UILabel{
        let titleFrameWidth = canvasWidth * 0.8
        let titleFrame = CGRect(x: (canvasWidth-titleFrameWidth)/2, y: 30, width: titleFrameWidth, height: canvasHeight)
        let label = UILabel(frame: titleFrame)
        label.text = text
        label.font = FontManager.gameFont(withSize: size)
        label.textAlignment = NSTextAlignment.Center
        label.textColor = FontManager.gameFontColor
        label.numberOfLines = 0
        FontManager.addFontShadow(label.layer)
        return label
    }
    
    func restartGame() {
        gameOverContainerView.hidden = true
        overlay.hidden = true
        engine.restartGame()
    }
}